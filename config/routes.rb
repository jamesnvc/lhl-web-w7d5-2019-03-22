Rails.application.routes.draw do
  # doing this just puts the routes for books under the author routes
  # as a consequence of this, the helper methods change, but all the
  # routes will still be going to their original controller (e.g.
  # /authors/1/books is still going to be calling the index method on
  # the book controller)
  resources :authors, only: [:index, :show] do
    resources :books
  end

  # resources :books, only: [:index]

  # this changes the routes to put the given resources under that path
  # (so this will, e.g. make /admin/authors/...) but unlike nested
  # resources, this also changes the controller that it goes to
  # so /admin/authors/new calls the new method of the
  # Admin::AuthorsController, not the original AuthorsController
  namespace :admin do
    resources :authors, except: [:index, :show]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
