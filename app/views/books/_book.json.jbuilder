json.extract! book, :id, :title, :genre, :author_id, :created_at, :updated_at
json.url author_book_url(@author, book, format: :json)
